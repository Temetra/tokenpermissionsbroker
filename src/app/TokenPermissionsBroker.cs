using System.Collections.Generic;

namespace ConsoleApplication
{
	public class TokenPermissionsBroker
	{
		Dictionary<string,List<string>> Permissions { get; }

		public TokenPermissionsBroker(Dictionary<string,List<string>> permissions)
		{
			Permissions = permissions;
		}

		public bool HasPermission(string token, string permissionRequired)
		{
			// Check token
			if (string.IsNullOrWhiteSpace(token))
				throw new System.ArgumentException("token should not be null or empty");

			// Check required permission
			if (string.IsNullOrWhiteSpace(permissionRequired))
				throw new System.ArgumentException("permissionRequired should not be null or empty");

			// Get permissions for token
			List<string> tokenPermissions = (Permissions.ContainsKey(token)) ? Permissions[token] : null;

			// Check permissions
			return (tokenPermissions != null) ? CheckPermissions(tokenPermissions, permissionRequired) : false;
		}

		bool CheckPermissions(List<string> tokenPermissions, string permissionRequired)
		{
			// False if no permissions set at all
			if (tokenPermissions.Count < 0) return false;

			// True if only permission is *
			if (tokenPermissions.Count == 1 && tokenPermissions[0] == "*") return true;

			// True if required permission is in List
			return tokenPermissions.Contains(permissionRequired);
		}	
	}
}