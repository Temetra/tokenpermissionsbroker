using System.Collections.Generic;

public class AppConfig
{
	public Dictionary<string, List<string>> Permissions { get; set; }
}