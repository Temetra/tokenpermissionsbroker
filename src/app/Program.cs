﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using static System.Console;

namespace ConsoleApplication
{
	public class Program
	{
		static IConfigurationRoot Configuration { get; set; }
		static IServiceProvider ServiceProvider { get; set; }
		static IOptions<AppConfig> AppConfig { get; set; }
		
		public static void Main(string[] args)
		{
			// Load config
			LoadConfiguration(args);

			// Run permissions check
			var permissionsBroker = new TokenPermissionsBroker(AppConfig.Value.Permissions);
			string who = "Token001";
			string what = "Action3";
			WriteLine($"{who} has permission for {what}? {permissionsBroker.HasPermission(who, what)}");
		}

		static void LoadConfiguration(string[] args)
		{
			// Load config
			Configuration = new ConfigurationBuilder()
				.AddEnvironmentVariables()
				.AddCommandLine(args)
				.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
				.Build();

			// Set up service provider
			ServiceProvider = new ServiceCollection()
				.AddOptions()
				.Configure<AppConfig>(Configuration.GetSection("AppConfig"))
				.BuildServiceProvider();

			// Create config instance
			AppConfig = ServiceProvider.GetService<IOptions<AppConfig>>();
		}
	}
}
